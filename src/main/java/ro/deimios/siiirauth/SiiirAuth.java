package ro.deimios.siiirauth;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.http.client.CookieStore;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

/**
 *
 * @author "Deimios" <deimios666@gmail.com>
 */
public class SiiirAuth {

    public static String DEFAULT_INDEX_URL = "https://www.siiir.edu.ro/siiir/login.jsp";
    public static String DEFAULT_LOGIN_URL = "https://www.siiir.edu.ro/siiir/j_spring_security_check";
    public static String DEFAULT_FETCH_URL = "";
    public static String DEFAULT_APPCONFIG_URL = "https://www.siiir.edu.ro/siiir/home/AMS/util/AppConfig.js";
    public static String DEFAULT_USERNAME = "CV_test";
    public static String DEFAULT_PASSWORD = "CV_test";
    public static String LOGIN_MESSAGE_OK = "{success:true}";

    public static Boolean checkLogin() {
        return checkLogin(DEFAULT_INDEX_URL, DEFAULT_LOGIN_URL, DEFAULT_USERNAME, DEFAULT_PASSWORD, DEFAULT_FETCH_URL);
    }

    public static Boolean checkLogin(String indexUrl, String loginUrl, String username, String password, String fetchUrl) {
        return doLogin(indexUrl, loginUrl, username, password, fetchUrl) != null;
    }

    public static JsonObject doLogin() {
        return doLogin(DEFAULT_INDEX_URL, DEFAULT_LOGIN_URL, DEFAULT_USERNAME, DEFAULT_PASSWORD, DEFAULT_FETCH_URL);
    }

    public static JsonObject doLogin(String indexUrl, String loginUrl, String username, String password, String fetchUrl) {
        Gson gson = new Gson();
        CookieStore cookieStore = new BasicCookieStore();
        CloseableHttpClient httpClient = HttpClients.custom()
                .setDefaultCookieStore(cookieStore)
                .build();

        //step 1 - get session cookie
        try {
            HttpGet getRequest = new HttpGet(indexUrl);
            CloseableHttpResponse getResponse = httpClient.execute(getRequest);
        } catch (IOException ex) {
        }

        //step 2 - send login data
        String bodyText = "";
        try {
            HttpPost postRequest = new HttpPost(loginUrl);
            postRequest.addHeader("Accept", "application/json");
            postRequest.addHeader("Accept-Charset", "UTF-8");
            postRequest.addHeader("charset", "UTF-8");
            postRequest.addHeader("X-Requested-With", "XMLHttpRequest");
            postRequest.addHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
            postRequest.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.80 Safari/537.36");
            StringEntity params = new StringEntity(
                    "j_username="
                    + URLEncoder.encode(username, "UTF-8")
                    + "&j_password="
                    + URLEncoder.encode(password, "UTF-8"));
            postRequest.setEntity(params);
            CloseableHttpResponse postResponse = httpClient.execute(postRequest);
            ResponseHandler<String> handler = new BasicResponseHandler();
            bodyText = handler.handleResponse(postResponse);

        } catch (IOException ex) {
        }

        //step 3 - analyze response and return
        if (!bodyText.equals(LOGIN_MESSAGE_OK)) {
            return null;
        }

        //step 4 - if fetch url is given execute fetch, else return empty Json
        if (fetchUrl.length() > 0) {

            //TODO IMPLEMENT FETCH AND REMOVE THIS
            return gson.fromJson("", JsonObject.class);
        } else {

            //TODO FETCH 
            try {
                HttpPost postRequest = new HttpPost(DEFAULT_APPCONFIG_URL);
                postRequest.addHeader("Accept", "application/json");
                postRequest.addHeader("Accept-Charset", "UTF-8");
                postRequest.addHeader("charset", "UTF-8");
                postRequest.addHeader("X-Requested-With", "XMLHttpRequest");
                postRequest.addHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
                postRequest.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.80 Safari/537.36");
                CloseableHttpResponse postResponse = httpClient.execute(postRequest);
                ResponseHandler<String> handler = new BasicResponseHandler();
                bodyText = handler.handleResponse(postResponse);
                Pattern pattern = Pattern.compile("\"code\":\"([0-9]*)\"");
                Matcher matcher = pattern.matcher(bodyText);
                if (matcher.find()) {
                    return gson.fromJson("{\"code\":" + matcher.group(1) + "ok}", JsonObject.class);
                }
            } catch (IOException ex) {
            }
            return gson.fromJson("{\"login\":\"ok\"}", JsonObject.class);
        }
    }

    public static void main(String[] args) {
        System.out.println("Running self check: ");
        System.out.println("Connection: " + checkLogin());
    }
}
